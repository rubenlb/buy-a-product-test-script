package digital.harbor;

import static org.junit.Assert.assertEquals;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class toBuyProductTest {
	
	private WebDriver driver;
	
	String user="rubenlb@prueba.com";
	String password="rlb123";
	
	By signInLocator=By.className("login");
	By registerPageLocator=By.xpath("//*[@id=\"columns\"]/div[1]/span[2]");
	
	By emailBoxLocator=By.id("email");
	By passBoxLocator=By.id("passwd");
	By loginButtonLocator=By.id("SubmitLogin");
	
	By tshirtsLinkLocator = By.xpath("//a[@title='T-shirts']");
	By tshirtLocator=By.partialLinkText("Faded Short Sleeve T-shirts");
	By addCartButtonLocator=By.name("Submit");
	By proceedCheckoutLocator = By.partialLinkText("Proceed to checkout");
	
	By shoppingPageLocator=By.id("cart_title");
	By proccedLinkLocator =By.partialLinkText("Proceed to checkout");
	By processAdressButtonLocator=By.name("processAddress");
	By processCarrierLocator=By.name("processCarrier");
	By agreeTermsLocator=By.id("cgv");
	By payByBankLinkLocator=By.partialLinkText("Pay by bank wire");
	By bankPaymentLocator= By.xpath("//*[contains(text(),'You have chosen to pay by bank wire.')]" );
	By confirmButtonLocator=By.xpath("//button[@class='button btn btn-default button-medium']");
	
	By buyConfirmedLocator=By.xpath("//*[contains(text(),'Your order on My Store is complete.')]" );
	
	@Before
	public void setup() {
		System.setProperty("webdriver.chrome.driver","src/test/resources/chromedriver/chromedriver.exe");
		
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://automationpractice.com/index.php");
	}
	@Test
	public void testBuyProduct() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		driver.findElement(signInLocator).click();
		
		if(driver.findElement(registerPageLocator).isDisplayed()) {
			// Log in
			driver.findElement(emailBoxLocator).sendKeys(user);
			driver.findElement(passBoxLocator).sendKeys(password);
			
			driver.findElement(loginButtonLocator).click();
			driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
			
			if (driver.getTitle().equals("My account - My Store")){
				// Select a T-shirt
				List<WebElement> tshirtsLinks = driver.findElements(tshirtsLinkLocator);
				tshirtsLinks.get(1).click();
				driver.findElement(tshirtLocator).click();
				// add to cart
				driver.findElement(addCartButtonLocator).click();
				driver.findElement(proceedCheckoutLocator).click();
				//Buy process
				
				if(driver.findElement(shoppingPageLocator).isDisplayed()) {
					driver.findElement(proccedLinkLocator).click();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					driver.findElement(processAdressButtonLocator).click();
					driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
					//accept agree Terms
					driver.findElement(agreeTermsLocator).click();
					
					driver.findElement(processCarrierLocator).click();
					driver.findElement(payByBankLinkLocator).click();
					
					if(driver.findElement(bankPaymentLocator).isDisplayed()) {
						driver.findElement(confirmButtonLocator).click();
						
						//Validation
						assertEquals("Order confirmation - My Store",driver.getTitle());
						
					}else{
						System.out.print("payment page was not found");
					}
				}
				
			}
			else {
				System.out.print("Incorrect User account- "+user+" - "+password);
			}
			
		}else {
			System.out.print("Login Page was not found  ");
		}
		
		
		
	}
	@After
	public void tearDown() {
		//driver.quit();
	}
}
