package digital.harbor.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Connector {

	private static WebDriver driver;
	
	public static WebDriver chromeDriverConnection() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver/chromedriver.exe");
		driver= new ChromeDriver();
		return driver;
	}
}
