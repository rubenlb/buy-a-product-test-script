package digital.harbor.pom;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class Buy_Test {
	private WebDriver driver;
	LoginPage loginPage;
	ChooseItemPage chooseItemPage;
	ShoppingPage shoppingPage;
	
	@Before
	public void setUp() throws Exception {
		driver=Connector.chromeDriverConnection();
		loginPage=new LoginPage(driver);
		chooseItemPage=new ChooseItemPage(driver);
		shoppingPage=new ShoppingPage(driver);
		//driver=loginPage.chromeDriverConnection();
		loginPage.visit("http://automationpractice.com/index.php");
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void test() throws InterruptedException {
		
		loginPage.loginUser();
		if(loginPage.loginSuccess()) {
			chooseItemPage.SelectItem();
			chooseItemPage.addToCartCheckout();
			if(shoppingPage.ShoppingPageSuccess()) {
				shoppingPage.buyProcess();
				assertEquals("Order confirmation - My Store",shoppingPage.pageTitle());
			}
		}
	}

}
