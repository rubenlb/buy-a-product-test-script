package digital.harbor.pom;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class Base {

	private WebDriver driver;
	
	public Base(WebDriver driver){
		this.driver= driver;		
	}
	/*
	public WebDriver chromeDriverConnection() {
		System.setProperty("webdriver.chrome.driver", "src/test/resources/chromedriver/chromedriver.exe");
		driver= new ChromeDriver();
		return driver;
	}*/
	
	public WebElement findElement(By locator) {
		return driver.findElement(locator);
	}
	
	public List<WebElement> findElements(By locator){
		return driver.findElements(locator);
	}
	public String getText(WebElement element) {
		return element.getText();
	}
	public String getText(By locator) {
		return this.findElement(locator).getText();
	}
	public void type(String inputText,By locator) {
		this.findElement(locator).sendKeys(inputText);
	}
	public void click(By locator) {
		this.findElement(locator).click();
	}
	public Boolean isDisplayed(By locator) {
		try {
			return this.findElement(locator).isDisplayed();
		}
		catch(org.openqa.selenium.NoSuchElementException e) {
			return false;
		}
	}
	public void visit(String url) {
		driver.get(url);
	}
	public String pageTitle(){
		return driver.getTitle();
	}
	
	public void implicitWait3Sec() {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}
}
