package digital.harbor.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ShoppingPage extends Base {

	By shoppingPageLocator=By.id("cart_title");
	By proccedLinkLocator =By.partialLinkText("Proceed to checkout");
	By processAdressButtonLocator=By.name("processAddress");
	By processCarrierLocator=By.name("processCarrier");
	By agreeTermsLocator=By.id("cgv");
	By payByBankLinkLocator=By.partialLinkText("Pay by bank wire");
	//By bankPaymentLocator= By.xpath("//*[contains(text(),'You have chosen to pay by bank wire.')]" );
	By confirmButtonLocator=By.xpath("//button[@class='button btn btn-default button-medium']");
	
	public ShoppingPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public Boolean ShoppingPageSuccess(){
		return isDisplayed(shoppingPageLocator);
		
	}
	public void buyProcess() {
		click(proccedLinkLocator);
		implicitWait3Sec();
		click(processAdressButtonLocator);
		implicitWait3Sec();
		//accept agree Terms
		click(agreeTermsLocator);
		
		click(processCarrierLocator);
		click(payByBankLinkLocator);
		
		click(confirmButtonLocator);
		
	}
}
