package digital.harbor.pom;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends Base {
	
	String user="rubenlb@prueba.com";
	String password="rlb123";

	By signInLocator=By.className("login");
	By registerPageLocator=By.xpath("//*[@id=\"columns\"]/div[1]/span[2]");
	
	By emailBoxLocator=By.id("email");
	By passBoxLocator=By.id("passwd");
	By loginButtonLocator=By.id("SubmitLogin");
	
	public LoginPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void loginUser() throws InterruptedException {
		click(signInLocator);
		Thread.sleep(2000);
		if(isDisplayed(registerPageLocator)) {
			type(user,emailBoxLocator);
			type(password,passBoxLocator);
			
			click(loginButtonLocator);
		}
		else {
			System.out.print("Login Page was not found");
		}
	}
	public Boolean loginSuccess(){
		return pageTitle().equals("My account - My Store");
	}

}
