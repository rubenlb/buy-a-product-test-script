package digital.harbor.pom;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ChooseItemPage extends Base {

	By tshirtsLinkLocator = By.xpath("//a[@title='T-shirts']");
	By tshirtLocator=By.partialLinkText("Faded Short Sleeve T-shirts");
	By addCartButtonLocator=By.name("Submit");
	By proceedCheckoutLocator = By.partialLinkText("Proceed to checkout");
	
	public ChooseItemPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	public void SelectItem() {
		List<WebElement> tshirtsLinks = findElements(tshirtsLinkLocator);
		tshirtsLinks.get(1).click();
		findElement(tshirtLocator).click();
	}
	
	public void addToCartCheckout() {
		click(addCartButtonLocator);
		implicitWait3Sec();
		click(proceedCheckoutLocator);
	}

}
